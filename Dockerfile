FROM node:fermium-alpine

WORKDIR /usr/src/app

COPY package.json yarn.lock ./

COPY docs/ ./docs/

RUN apk upgrade && \
    yarn install

CMD ["/bin/sh"]
